const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
let Schedule = require('../pojo/schedule');

let MeetingSchema = new Schema({
    submissionTime: {
        type: Date,
        required: true
    },
    employeeId: {
        type: String,
        required: true
    },
    startDate: {
        type: String,
        required: true
    },
    startTime: {
        type: String,
        required: true
    },
    endTime: {
        type: String,
        required: true
    }
});

MeetingSchema.methods.isOverlap = function () {
    let that = this;
    return new Promise((resolve, reject) => {
       that.constructor
           .find({
               startDate: that.startDate,
           })
           .exec()
           .then((meetings) => {
               let resultSet = meetings.reduce((list, elem) => {
                   if ((elem.startTime < that.endTime && elem.endTime > that.startTime)
                   || (elem.endTime > that.startTime && elem.startTime < that.startTime)) {
                       list.push(elem);
                   }
                   return list;
               }, []);
               let result = resultSet.length !== 0;
               resolve(result);
           })
   });
};

MeetingSchema.methods.overlaps = function (another) {
    return ((this.startTime < another.endTime && this.endTime > another.startTime)
        || (this.endTime > another.startTime && this.startTime < another.startTime));
};

MeetingSchema.methods.isOvertime = function () {
    return this.startTime < Schedule.getOpeningTime()
        || this.endTime > Schedule.getClosingTime();
};

MeetingSchema.statics.sortedMeetings = function () {
    let that = this;
    return new Promise((resolve, reject) => {
        that.distinct("startDate").exec()
            .then((receivedDates) => {
            if (receivedDates.length === 0) {
                reject();
            }
                let resultMeetings = [];
                let resultSet = [];
                function counterController(result) {
                    resultSet.push(result);
                    if(resultSet.length === receivedDates.length) {
                        resultSet.sort((a, b) => {
                            return a.meetingDate.localeCompare(b.meetingDate);
                        });
                        resolve(resultSet);
                    }
                }
                receivedDates.forEach((date) => {
                    that.find({startDate: date}).exec()
                        .then((receivedMeetings) => {
                            resultMeetings = receivedMeetings
                                .sort((a, b) => {
                                    return a.startTime.localeCompare(b.startTime);
                                })
                                .map((meeting) => {
                                    return meeting.body;
                                });
                            counterController({
                                meetingDate: date,
                                meetings: resultMeetings
                            });
                        })
                })
            });
    });
};

MeetingSchema.virtual('body')
    .set(function (meeting) {
        let startTimeArray = meeting.startTime.split(' ');
        this.submissionTime = meeting.submissionTime;
        this.startDate = startTimeArray[0];
        this.startTime = startTimeArray[1];
        let endTimeArray = startTimeArray[1].split(':');
        endTimeArray[0] = parseInt(endTimeArray[0]) + meeting.duration;
        this.endTime = endTimeArray.join(':');
        this.employeeId = meeting.employeeId;
    })
    .get(function () {
        return {
            startTime: this.startTime,
            endTime: this.endTime,
            employeeId: this.employeeId
        }
    });

module.exports = mongoose.model('Meeting', MeetingSchema);