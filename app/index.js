const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Meeting = require('./models/meeting');
let Schedule = require('./pojo/schedule');

const db = 'mongodb://localhost/booking';
mongoose.connect(db);

const port = 3000;

app.listen(port, function () {
    console.log("connected...");
});

app.get('/', function (req, res) {
    res.send('Hello!\nPlease go to "/schedule" to get booking information');
});

app.get('/schedule', function (req, res) {
    Meeting.sortedMeetings()
        .then((results) => {
        Schedule.setMeetings(results);
        res.json(Schedule.print());
    }).catch(() => {
        res.json(Schedule.print());
    })
});

app.get('/schedule/working-hours', function (req, res) {
    res.json(Schedule.getWorkingHours());
});

app.post('/schedule/working-hours', function (req, res) {
    Schedule.setWorkingHours(req.body.openingTime, req.body.closingTime);
    res.json(Schedule.print());
});

app.get('/schedule/meetings', function (req, res) {
    Meeting.sortedMeetings().then((results) => {
        res.send(results);
    }).catch(() => {
        res.end();
    })
});

app.post('/schedule/meetings', function (req, res) {
    if (Schedule.working()) {
        req.body
            .sort((a, b) => {
                return a.submissionTime.localeCompare(b.submissionTime);
            })
            .map((meeting) => {
                let newMeeting = new Meeting();
                newMeeting.body = meeting;
                return newMeeting;
            })
            .reduce((list, elem) => {
                let overlap = true;
                list.forEach((meeting) => {
                    if (elem.startDate === meeting.startDate
                        && ((elem.startTime < meeting.endTime && elem.endTime > meeting.startTime)
                        || (elem.endTime > meeting.startTime && elem.startTime < meeting.startTime))) {
                        overlap = false;
                    }
                });
                if (overlap) {
                    list.push(elem);
                }
                return list;
            },[])
            .forEach((meeting) => {
                if (!meeting.isOvertime()) {
                    meeting.isOverlap()
                        .then((result) => {
                            if (!result) {
                                meeting.save(function (err, meeting) {
                                    if (err) {
                                        res.json(err);
                                    }
                                });
                            }
                        })
                }
            });
        res.end();
    } else {
        res.end();
    }
});