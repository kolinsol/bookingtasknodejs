const schedule = (function () {

    let openingTime;
    let closingTime;
    let meetings;
    let instance;

    const getOpeningTime = function() {
        return openingTime;
    };

    const getClosingTime = function() {
        return closingTime;
    };

    const getMeetings = function() {
        return meetings
    };

    const setWorkingHours = function(newOpeningTime, newClosingTime) {
        if (openingTime === undefined && closingTime === undefined) {
            openingTime = newOpeningTime;
            closingTime = newClosingTime;
        }
    };

    const getWorkingHours = function () {
        return {
            openingTime: openingTime,
            closingTime: closingTime
        }
    };

    const working = function () {
        return openingTime !== undefined
            && closingTime !== undefined;
    };

    const setMeetings = function(newMeetings) {
        meetings = newMeetings
    };

    const print = function() {
        return {
            workingHours: {
                openingTime: openingTime,
                closingTime: closingTime
            },
            meetings: meetings
        }
    };

    const createInstance = function () {
        return {
            getOpeningTime: getOpeningTime,
            getClosingTime: getClosingTime,
            getMeetings: getMeetings,
            getWorkingHours: getWorkingHours,
            setWorkingHours: setWorkingHours,
            setMeetings: setMeetings,
            working: working,
            print: print
        }
    };

    return {
        getInstance: function () {
            return instance || (instance = createInstance());
        }
    }
}());

module.exports = schedule.getInstance();